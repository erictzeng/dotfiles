set fish_greeting
set -x ALTERNATE_EDITOR ""
set -x EDITOR "emacsclient -nw"

if test -d ~/.local/bin; set -x PATH $HOME/.local/bin $PATH; end
if test -d ~/.pyenv; set -x PATH $HOME/.pyenv/bin $PATH; end

if command -sq pyenv; status --is-interactive; and source (pyenv init -|psub); end
if command -sq pyenv; status --is-interactive; and source (pyenv virtualenv-init -|psub); end

if command -sq direnv; eval (direnv hook fish); end
